import { useState, useEffect, useCallback } from 'react';
import useHttp from '../../hooks/use-http';
import classes from './Comments.module.css';
import NewCommentForm from './NewCommentForm';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { getAllComments } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import CommentsList from './CommentsList';


const Comments = () => {
  const [isAddingComment, setIsAddingComment] = useState(false);
  const params = useParams();
  const {id} = params;
  const { sendRequest, status, data: loadedComments } = useHttp(getAllComments);

  useEffect(()=>{
    sendRequest(id);
  },[sendRequest, id]);

  const startAddCommentHandler = () => {
    setIsAddingComment(true);
  };
  
  const onAddedComment = useCallback(() => {
    sendRequest(id);
  },[sendRequest, id]);

  let comments;
  
  if(status === 'pending') {
    comments = (<div className="centered">
    <LoadingSpinner/>
  </div>);
  }

  if(status === 'completed' && loadedComments) {
    comments = <CommentsList comments={loadedComments}/>;
  }

  if(status === 'completed' && (!loadedComments || loadedComments.length === 0)) {
    comments = <p className='centered'>No comments were added yet!</p>;
  }

  return (
    <section className={classes.comments}>
      <h2>User Comments</h2>
      {!isAddingComment && (
        <button className='btn' onClick={startAddCommentHandler}>
          Add a Comment
        </button>
      )}
      {isAddingComment && <NewCommentForm quoteId={params.id} onAddedComment={onAddedComment}/>}
      {comments}
    </section>
  );
};

export default Comments;
