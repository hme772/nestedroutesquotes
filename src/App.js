import React from "react";
import { Route, Switch } from "react-router-dom";
import AllQuotes from "./pages/AllQuotes";
import AddQuote from "./pages/AddQuote";
import DetailedQuote from "./pages/DetailedQuote";
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";
import Layout from "./components/layout/Layout";
import NotFound from "./pages/NotFound";

function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          <Redirect to="/quotes" />
        </Route>
        <Route path="/quotes" exact>
          <AllQuotes />
        </Route>
        <Route path="/new-quote">
          <AddQuote />
        </Route>
        <Route path="/quotes/:id">
          <DetailedQuote />
        </Route>
        <Route path='*'>
          <NotFound/>
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
