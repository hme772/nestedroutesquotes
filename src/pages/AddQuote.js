import { useEffect } from 'react';
import QuoteForm from '../components/quotes/QuoteForm';
import { useHistory } from 'react-router-dom'; 
import useHttp from '../hooks/use-http';
import { addQuote } from '../lib/api';

const AddQuote = () => {
    const history = useHistory();
    const {sendRequest, status} = useHttp(addQuote);

    useEffect(()=>{
        if (status === 'completed') {
            history.push('/quotes');
        }
    },[status, history]);

    const onAddQuote = (data) => {
        sendRequest(data);
    };

    return <QuoteForm isLoading={status === 'pending'} onAddQuote={onAddQuote}/>;
}

export default AddQuote;