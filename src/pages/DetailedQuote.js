import { useParams, Route, Link, useRouteMatch } from "react-router-dom";
import Comments from "../components/comments/Comments";
import HighlightedQuote from "../components/quotes/HighlightedQuote";
import { useEffect } from "react";
import useHttp from "../hooks/use-http";
import { getSingleQuote } from "../lib/api";
import LoadingSpinner from "../components/UI/LoadingSpinner";


const DetailedQuote = () => {
    const params = useParams();
    const {id} = params;
    const match = useRouteMatch();
    const {sendRequest, status, data: loadedQuote, error} = useHttp(getSingleQuote, true);

    useEffect(()=>{
        sendRequest(id);
    },[sendRequest, id]);

    if(status === 'pending') {
        return <div className="centered">
          <LoadingSpinner/>
        </div>
      }
    
      if(error) {
        return <div className="centered focused">
          <p>{error}</p>
        </div>
      }

    if(!loadedQuote.text) {
        return <p>No quote found!</p>
    }

    return <>
    <HighlightedQuote author={loadedQuote.author} text={loadedQuote.text}/>
    <div className='centered'>
    <Route path={`${match.path}`} exact>
        <Link className='btn--flat' to={`${match.url}/comments`}>Load Comments</Link>
    </Route>
    </div>
    <Route path={`${match.path}/comments`}>
        <Comments/> 
    </Route>
    </>;
}

export default DetailedQuote;